// console.log("Hello World");

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends:{
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	},
	talk: function(){
		console.log(this.pokemon[0]+"! I choose you!");
	}
}

console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer['pokemon']);

console.log("Result of talk method:");
trainer.talk();

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;
	this.tackle = function(targetPokemon){
		console.log(this.name + " tackled " + targetPokemon.name)
		this.newHealth = targetPokemon.health - this.attack
		console.log(targetPokemon.name+"'s health is now reduced to " + this.newHealth);
		if (this.newHealth <= 0){
			targetPokemon.faint();
		};
		console.log(targetPokemon);
	}
	this.faint = function(){
			console.log(this.name + " fainted.");
	}
}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

let mewtwo = new Pokemon("MewTwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);

mewtwo.tackle(geodude);





